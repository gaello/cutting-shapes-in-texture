﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

/// <summary>
/// Input layer reads pointer actions on the screen.
/// </summary>
public class InputLayer : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler
{
    /// <summary>
    /// Action called when user press pointer.
    /// </summary>
    public UnityAction<Vector2> PointerDown;

    /// <summary>
    /// Unity OnPointerDown event.
    /// </summary>
    /// <param name="eventData">Event data.</param>
    public void OnPointerDown(PointerEventData eventData)
    {
        PointerDown?.Invoke(eventData.position);
    }


    /// <summary>
    /// Action called when user begin drag.
    /// </summary>
    public UnityAction<Vector2> DragBegin;

    /// <summary>
    /// Unity OnBeginDrag event.
    /// </summary>
    /// <param name="eventData">Event data.</param>
    public void OnBeginDrag(PointerEventData eventData)
    {
        DragBegin?.Invoke(eventData.position);
    }


    /// <summary>
    /// Action called when user continue drag.
    /// </summary>
    public UnityAction<Vector2> Drag;

    /// <summary>
    /// Unity OnDrag event.
    /// </summary>
    /// <param name="eventData">Event data.</param>
    public void OnDrag(PointerEventData eventData)
    {
        Drag?.Invoke(eventData.position);
    }


    /// <summary>
    /// Action called when user end drag.
    /// </summary>
    public UnityAction<Vector2> DragEnd;

    /// <summary>
    ///Unity OnEndDrag event.
    /// </summary>
    /// <param name="eventData">Event data.</param>
    public void OnEndDrag(PointerEventData eventData)
    {
        DragEnd?.Invoke(eventData.position);
    }
}

﻿using UnityEngine;

/// <summary>
/// This class control the texture and removing of its parts.
/// </summary>
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class TextureController : MonoBehaviour
{
    // Amount of pixels per unit on the scene.
    public int pixelsPerUnit = 50;
    // Size of single pixel on the texture.
    private float pixelSize;

    // Texture width.
    private int textureWidth;
    // Texture height.
    private int textureHeight;

    // Mesh width.
    private float meshWidth;
    // Mesh height
    private float meshHeight;

    // Reference to the MeshRenderer.
    private MeshRenderer meshRenderer;

    /// <summary>
    /// Unity method called on object creation.
    /// </summary>
    private void Awake()
    {
        // Setting reference to the Mesh Renderer.
        meshRenderer = GetComponent<MeshRenderer>();

        // Gathering values for variables.
        meshWidth = transform.localScale.x;
        meshHeight = transform.localScale.y;

        // Calculating texture variables.
        pixelSize = 1f / pixelsPerUnit;

        textureWidth = Mathf.RoundToInt(meshWidth / pixelSize);
        textureHeight = Mathf.RoundToInt(meshHeight / pixelSize);
    }

    /// <summary>
    /// Unity method called on the first frame.
    /// </summary>
    void Start()
    {
        GenerateTexture();
    }

    /// <summary>
    /// Generating texture for the mesh.
    /// </summary>
    private void GenerateTexture()
    {
        // Texture initialization.
        var newTexture = new Texture2D(textureWidth, textureHeight, TextureFormat.RGBA32, false);

        // Setting texture variables.
        newTexture.alphaIsTransparency = true;
        newTexture.filterMode = FilterMode.Point;

        // Creating array of colors for the texture.
        var colors = new Color[textureWidth * textureHeight];
        for (int y = 0; y < textureHeight; y++)
        {
            for (int x = 0; x < textureWidth; x++)
            {
                colors[y * textureWidth + x] = Color.white;
            }
        }

        // Setting colors in the texture and applying changes.
        newTexture.SetPixels(colors);
        newTexture.Apply(false);

        // Assigning texture to the material.
        meshRenderer.material.mainTexture = newTexture;
    }

    /// <summary>
    /// Erases the texture part.
    /// </summary>
    /// <param name="position">Position in world space.</param>
    /// <param name="range">Removing range.</param>
    public void EraseTexturePart(Vector2 position, float range)
    {
        // Converting world position to the pixel indexes.
        var meshStart = new Vector2(-meshWidth / 2, -meshHeight / 2);
        var translatedPosition = position - meshStart;
        translatedPosition /= pixelSize;

        // Converting range to the pixel indexes.
        var translatedRange = range / pixelSize;

        // Calculating square in which pixels are being removed.
        // Width
        int startX = Mathf.CeilToInt(Mathf.Max(0, translatedPosition.x - translatedRange));
        int endX = Mathf.FloorToInt(Mathf.Min(textureWidth, translatedPosition.x + translatedRange));
        int diffX = Mathf.Max(0, endX - startX);

        // Height
        int startY = Mathf.CeilToInt(Mathf.Max(0, translatedPosition.y - translatedRange));
        int endY = Mathf.FloorToInt(Mathf.Min(textureHeight, translatedPosition.y + translatedRange));
        int diffY = Mathf.Max(0, endY - startY);

        // Getting reference to the texture.
        var texture = meshRenderer.material.mainTexture as Texture2D;
        // Getting array of colors from the texture to change.
        var colors = texture.GetPixels(startX, startY, diffX, diffY);

        var centerPos = translatedPosition;
        for (int y = 0; y < diffY; y++)
        {
            for (int x = 0; x < diffX; x++)
            {
                // Checking if element is in removing range.
                var pointPos = new Vector2(startX + x, startY + y);
                if (Vector2.Distance(pointPos, centerPos) > translatedRange)
                {
                    continue;
                }

                // Removing color from the array.
                colors[y * diffX + x] = new Color(0, 0, 0, 0);
            }
        }

        // Applying changes back to the texture.
        texture.SetPixels(startX, startY, diffX, diffY, colors);
        texture.Apply(false);
    }
}

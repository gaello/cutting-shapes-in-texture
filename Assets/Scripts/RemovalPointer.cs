﻿using UnityEngine;

/// <summary>
/// Removal Pointer.
/// Calculates world position of user input and passes it to the texture.
/// </summary>
public class RemovalPointer : MonoBehaviour
{
    // Reference to the input layer to attach actions.
    public InputLayer inputLayer;
    // Reference to the mesh controller.
    public TextureController texture;

    // Reference to the main camera.
    public Camera main;

    // Removal radius.
    public float hitRadius = 2;

    /// <summary>
    /// Unity method called when component is enabled.
    /// </summary>
    private void OnEnable()
    {
        inputLayer.PointerDown += OnPointerDown;
        inputLayer.DragBegin += OnDragBegin;
        inputLayer.Drag += OnDrag;
        inputLayer.DragEnd += OnDragEnd;
    }

    /// <summary>
    /// Unity method called when component is disabled.
    /// </summary>
    private void OnDisable()
    {
        inputLayer.PointerDown -= OnPointerDown;
        inputLayer.DragBegin -= OnDragBegin;
        inputLayer.Drag -= OnDrag;
        inputLayer.DragEnd -= OnDragEnd;
    }

    /// <summary>
    /// Action on begin drag.
    /// </summary>
    /// <param name="screenPosition">Screen position.</param>
    private void OnDragBegin(Vector2 screenPosition)
    {
        RemovePartsOfTextureAt(screenPosition);
    }

    /// <summary>
    /// Action on drag.
    /// </summary>
    /// <param name="screenPosition">Screen position.</param>
    private void OnDrag(Vector2 screenPosition)
    {
        RemovePartsOfTextureAt(screenPosition);
    }

    /// <summary>
    /// Action on end drag.
    /// </summary>
    /// <param name="screenPosition">Screen position.</param>
    private void OnDragEnd(Vector2 screenPosition)
    {
        RemovePartsOfTextureAt(screenPosition);
    }

    /// <summary>
    /// Action on pointer down.
    /// </summary>
    /// <param name="screenPosition">Screen position.</param>
    private void OnPointerDown(Vector2 screenPosition)
    {
        RemovePartsOfTextureAt(screenPosition);
    }

    /// <summary>
    /// Removes parts of the texture at Screen Position.
    /// </summary>
    /// <param name="screenPosition">Screen position.</param>
    private void RemovePartsOfTextureAt(Vector2 screenPosition)
    {
        // Calculating world position.
        var worldPoint = main.ScreenToWorldPoint(screenPosition);
        var hitPoint = new Vector2(worldPoint.x, worldPoint.y);

        // Passing data to the texture to remove its parts.
        texture.EraseTexturePart(hitPoint, hitRadius);
    }
}

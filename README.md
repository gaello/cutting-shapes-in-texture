# Cutting Shapes in Mesh in Unity

This repository contain simple example how you can cut shapes in texture.

In addition to this repository I also made a post about it that you can find here: https://www.patrykgalach.com/2019/09/16/cutting-shapes-texture/

Enjoy!

---

# How to use it?

Clone this repository on your local drive and open project with Unity.

If you are interested only in seeing implementation, go straight to [Assets/Scripts/](https://bitbucket.org/gaello/cutting-shapes-in-texture/src/master/Assets/Scripts/) folder. You will find all code that I wrote to make it work. Code also have comments so it would make a little bit more sense.

I hope you will enjoy it!

---

For more visit my blog: https://www.patrykgalach.com
